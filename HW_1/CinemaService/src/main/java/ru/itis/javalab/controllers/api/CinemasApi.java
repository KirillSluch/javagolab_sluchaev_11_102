package ru.itis.javalab.controllers.api;

import ru.itis.javalab.dto.CinemaDto;
import ru.itis.javalab.dto.CinemasPage;
import ru.itis.javalab.dto.NewCinemaDto;
import ru.itis.javalab.dto.StandardResponseDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.javalab.validation.dto.ValidationErrorsDto;

@Tags(value =
@Tag(name = "Cinemas"))
@RequestMapping("/api/cinemas")
public interface CinemasApi {
    @Operation(summary = "Получение списка кинотеатров", description = "Доступно всем пользователям")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Запрос обработан успешно",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = CinemasPage.class))
                    })
    })
    @GetMapping
    ResponseEntity<CinemasPage> getCinemas(@Parameter(description = "Номер страницы", example = "1")
                                         @RequestParam("page") Integer page);

    @Operation(summary = "Получение списка кинотеатров в городе", description = "Доступно всем пользователям")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Запрос обработан успешно",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = CinemasPage.class))
                    })
    })
    @GetMapping("/city")
    ResponseEntity<CinemasPage> getCinemas(@Parameter(description = "Номер страницы", example = "1")
                                           @RequestParam("page") Integer page,
                                           @RequestParam("city") String city);

    @Operation(summary = "Получение информации о кинотеатре", description = "Доступно всем пользователям")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Запрос обработан успешно",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = CinemaDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "Кинотеатр не найден",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = StandardResponseDto.class))
                    })
    })
    @GetMapping("/{cinema-id}")
    ResponseEntity<CinemaDto> getCinema(@Parameter(description = "Идентификатор кинотеатра", example = "1")
                                    @PathVariable("cinema-id") Long cinemaId);

    @Operation(summary = "Добавление кинотеатра", description = "Доступно только менеджерам")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Задача добавлена успешно",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = CinemaDto.class))
                    }),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = ValidationErrorsDto.class))
                    })
    })
    @PostMapping
    ResponseEntity<CinemaDto> addCinema(@RequestBody @Valid NewCinemaDto newCinema);
}


