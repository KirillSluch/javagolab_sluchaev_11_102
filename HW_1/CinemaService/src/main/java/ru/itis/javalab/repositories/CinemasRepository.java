package ru.itis.javalab.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import ru.itis.javalab.models.Cinema;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CinemasRepository extends JpaRepository<Cinema, Long> {
    Optional<Cinema> findByTitle(String title);

    Page<Cinema> findAllByCity(PageRequest request, String city);

}
