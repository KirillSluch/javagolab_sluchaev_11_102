package ru.itis.javalab.controllers;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.javalab.controllers.api.CinemasApi;
import ru.itis.javalab.dto.CinemaDto;
import ru.itis.javalab.dto.CinemasPage;
import ru.itis.javalab.dto.NewCinemaDto;
import org.springframework.http.ResponseEntity;
import ru.itis.javalab.services.CinemasService;

@RestController
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class CinemasController implements CinemasApi {
    CinemasService cinemasService;

    @Override
    public ResponseEntity<CinemasPage> getCinemas(Integer page) {
        return ResponseEntity.ok(cinemasService.getCinema(page));
    }

    @Override
    public ResponseEntity<CinemasPage> getCinemas(Integer page, String city) {
        return ResponseEntity.ok(cinemasService.getCinema(page, city));
    }

    @Override
    public ResponseEntity<CinemaDto> getCinema(Long cinemaId) {
        return ResponseEntity.ok(cinemasService.getCinema(cinemaId));
    }

    @Override
    public ResponseEntity<CinemaDto> addCinema(NewCinemaDto newCinema) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(cinemasService.addCinema(newCinema));
    }
}
