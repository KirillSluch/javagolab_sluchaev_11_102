package ru.itis.javalab.services.impl;

import ru.itis.javalab.dto.CinemaDto;
import ru.itis.javalab.dto.CinemasPage;
import ru.itis.javalab.dto.NewCinemaDto;
import ru.itis.javalab.exceptions.RestException;
import jakarta.transaction.Transactional;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import ru.itis.javalab.models.Cinema;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.itis.javalab.repositories.CinemasRepository;
import ru.itis.javalab.services.CinemasService;

import java.util.Optional;

import static ru.itis.javalab.dto.CinemaDto.from;
import static ru.itis.javalab.security.utils.PageUtils.DEFAULT_PAGE_SIZE;

@Service
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class CinemasServiceImpl implements CinemasService {
    CinemasRepository cinemasRepository;

    @Override
    public CinemasPage getCinema(Integer page) {
        PageRequest request = PageRequest.of(page, DEFAULT_PAGE_SIZE, Sort.by("id"));

        Page<Cinema> cinemas = cinemasRepository.findAll(request);

        return CinemasPage.builder()
                .cinemas(from(cinemas.getContent()))
                .totalPages(cinemas.getTotalPages())
                .build();
    }

    @Override
    public CinemasPage getCinema(Integer page, String city) {
        PageRequest request = PageRequest.of(page, DEFAULT_PAGE_SIZE, Sort.by("id"));

        Page<Cinema> cinemas = cinemasRepository.findAllByCity(request, city);
        return CinemasPage.builder()
                .cinemas(from(cinemas.getContent()))
                .totalPages(cinemas.getTotalPages())
                .build();
    }

    @Override
    public CinemaDto getCinema(Long cinemaId) {
        Cinema cinema = cinemasRepository.findById(cinemaId)
                .orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "Cinema with id <" + cinemaId + "> not found"));
        return from(cinema);
    }

    @Override
    public CinemaDto getCinema(String title) {
        Optional<Cinema> cinema = cinemasRepository.findByTitle(title);

        return cinema.map(CinemaDto::from).orElse(null);
    }

    @Transactional
    @Override
    public CinemaDto addCinema(NewCinemaDto newCinema) {
        CinemaDto dto = this.getCinema(newCinema.getTitle());
        if (dto != null) {
            throw new RestException(HttpStatus.BAD_REQUEST, "Cinema with title <" + newCinema.getTitle() + "> already exists");
        }
        Cinema cinema = Cinema.builder()
                .title(newCinema.getTitle())
                .address(newCinema.getAddress())
                .city(newCinema.getCity())
                .build();

        cinemasRepository.save(cinema);

        return from(cinema);
    }
}
