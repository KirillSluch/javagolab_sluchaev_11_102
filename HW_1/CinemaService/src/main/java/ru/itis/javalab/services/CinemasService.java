package ru.itis.javalab.services;

import ru.itis.javalab.dto.CinemaDto;
import ru.itis.javalab.dto.CinemasPage;
import ru.itis.javalab.dto.NewCinemaDto;

public interface CinemasService {
    CinemasPage getCinema(Integer page);

    CinemasPage getCinema(Integer page, String city);

    CinemaDto getCinema(String title);

    CinemaDto getCinema(Long cinemaId);

    CinemaDto addCinema(NewCinemaDto newCinema);

}
