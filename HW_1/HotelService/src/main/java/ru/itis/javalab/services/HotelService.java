package ru.itis.javalab.services;

import ru.itis.javalab.dto.HotelDto;
import ru.itis.javalab.dto.HotelsPage;
import ru.itis.javalab.dto.NewHotelDto;

public interface HotelService {
    HotelsPage getHotel(Integer page);

    HotelDto getHotel(Long hotelId);
    HotelDto getHotel(String title);

    HotelsPage getHotel(Integer page, String city);

    HotelDto addHotel(NewHotelDto newHotel);

}
