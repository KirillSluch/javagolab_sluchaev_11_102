package ru.itis.javalab.security.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PageUtils {

    public static int DEFAULT_PAGE_SIZE;

    @Value("${hotel.page.size}")
    public void setDefaultPageSize(int defaultPageSize) {
        DEFAULT_PAGE_SIZE = defaultPageSize;
    }

}

