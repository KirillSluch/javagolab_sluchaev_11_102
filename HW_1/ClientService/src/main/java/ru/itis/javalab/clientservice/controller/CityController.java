package ru.itis.javalab.clientservice.controller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.javalab.clientservice.dto.CinemasPage;
import ru.itis.javalab.clientservice.dto.CityPage;
import ru.itis.javalab.clientservice.dto.HotelsPage;
import ru.itis.javalab.clientservice.service.CinemaService;
import ru.itis.javalab.clientservice.service.HotelService;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@RestController
public class CityController {

    CinemaService cinemasService;
    HotelService hotelsService;

    @GetMapping("/info")
    public ResponseEntity<CityPage> getInfo(@RequestParam("city") String city,
                                            @RequestParam("hotels_page") Integer hotelsPageNumber,
                                            @RequestParam("cinemas_page") Integer cinemasPageNumber) {
        CinemasPage cinemasPage = cinemasService.getCinemas(cinemasPageNumber, "cinema-key", city);
        HotelsPage hotelsPage = hotelsService.getHotels(hotelsPageNumber, "hotels-key", city);

        return ResponseEntity.ok(CityPage.builder()
                        .cinemasPage(cinemasPage)
                        .hotelsPage(hotelsPage)
                        .build());
    }
}

